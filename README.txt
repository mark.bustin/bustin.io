Welcome to bustin.io codebase!

This is the main Django project that will contain a variety of applications for whatever it is that I want to work on or use.

*******system prerequisites********
Python==3.8.5
geckodriver==0.28.0
*******venv prerequisites********
django==3.1.4
selenium==3.141.0
