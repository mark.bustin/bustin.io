from .base import FunctionalTest


class NewVisitorTest(FunctionalTest):

    def test_can_access_parity_page(self):
        print("**********")
        print("Testing Parity")
        print("**********")
        # Ben navigates to bustin.io
        self.browser.get(self.live_server_url)

        # Ben observes that bustin.io is actually a portal for Mark's online presence
        self.assertIn('bustin.io', self.browser.title)

        # Ben is happy to be greeted by a landing page with some basic content
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Toast?', header_text)
