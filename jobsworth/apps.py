from django.apps import AppConfig


class JobsworthConfig(AppConfig):
    name = 'jobsworth'
