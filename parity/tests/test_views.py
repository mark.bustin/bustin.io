from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.utils.html import escape
from unittest import skip

from parity.views import stat_tracker
# from parity.models import Item, List
# from parity.forms import (
#     DUPLICATE_ITEM_ERROR, EMPTY_ITEM_ERROR,
#     ExistingListItemForm, ItemForm,
# )

# Create your tests here.


class HomePageTest(TestCase):

    def test_uses_home_template(self):
        response = self.client.get('/parity')
        self.assertTemplateUsed(response, 'stat_tracker.html')
