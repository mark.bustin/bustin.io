from django.urls import path, include
from parity import views

urlpatterns = [
    path('', views.stat_tracker)
]
