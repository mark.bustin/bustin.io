from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.core.exceptions import ValidationError


def stat_tracker(request):
    return render(request, 'stat_tracker.html')
